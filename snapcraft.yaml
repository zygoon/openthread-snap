# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: Huawei Inc.

name: openthread
base: core22
summary: OpenThread Daemon
description: |
  OpenThread Daemon
grade: stable
confinement: strict
license: BSD-3-Clause
adopt-info: openthread

parts:
  openthread:
    plugin: nil
    build-packages:
      - cmake
      - ninja-build
      - generate-ninja
      - libmbedtls-dev
      - python3
    source: https://github.com/openthread/openthread.git
    source-type: git
    override-pull: |
      craftctl default
      # Set defaults
      grade=devel
      tag="$(git describe --tags --abbrev=0)" || true
      hash="$(git rev-parse --short HEAD)"
      # Check for tagged version
      if [ -n "$tag" ]; then
        count="$(git rev-list "$tag".. --count)"
        if [ "$count" -eq 0 ]; then
          version="$tag"
          grade=stable
        else
          version="$(echo "$tag" | sed -e 's/thread-reference-//')+git$count.$hash"
        fi
      else
        count="$(git rev-list HEAD --count)"
        version="0+git$count.$hash"
      fi
      # Relay back to snapcraft
      set -x
      craftctl set grade="$grade"
      craftctl set version="$version"
      set +x
    override-build: |
      mkdir -p build
      cd build
      cmake \
          -GNinja \
          -DCMAKE_INSTALL_PREFIX:PATH=/usr \
          -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
          -DOT_COMPILE_WARNING_AS_ERROR=ON \
          -DOT_PLATFORM=posix \
          -DOT_SLAAC=ON \
          -DOT_LOG_OUTPUT=PLATFORM_DEFINED \
          -DOT_POSIX_MAX_POWER_TABLE=ON \
          -DOT_ANYCAST_LOCATOR=ON \
          -DOT_BORDER_AGENT=ON \
          -DOT_BORDER_ROUTER=ON \
          -DOT_COAP=ON \
          -DOT_COAP_BLOCK=ON \
          -DOT_COAP_OBSERVE=ON \
          -DOT_COAPS=ON \
          -DOT_COMMISSIONER=ON \
          -DOT_CHANNEL_MANAGER=ON \
          -DOT_CHANNEL_MONITOR=ON \
          -DOT_CHILD_SUPERVISION=ON \
          -DOT_DATASET_UPDATER=ON \
          -DOT_DHCP6_CLIENT=ON \
          -DOT_DHCP6_SERVER=ON \
          -DOT_DIAGNOSTIC=ON \
          -DOT_DNS_CLIENT=ON \
          -DOT_ECDSA=ON \
          -DOT_HISTORY_TRACKER=ON \
          -DOT_IP6_FRAGM=ON \
          -DOT_JAM_DETECTION=ON \
          -DOT_JOINER=ON \
          -DOT_LEGACY=ON \
          -DOT_MAC_FILTER=ON \
          -DOT_MTD_NETDIAG=ON \
          -DOT_NEIGHBOR_DISCOVERY_AGENT=ON \
          -DOT_NETDATA_PUBLISHER=ON \
          -DOT_PING_SENDER=ON \
          -DOT_REFERENCE_DEVICE=ON \
          -DOT_SERVICE=ON \
          -DOT_SNTP_CLIENT=ON \
          -DOT_SRP_CLIENT=ON \
          -DOT_LOG_LEVEL_DYNAMIC=ON \
          -DOT_COMPILE_WARNING_AS_ERROR=ON \
          -DOT_RCP_RESTORATION_MAX_COUNT=2 \
          -DOT_UPTIME=ON \
          -DOT_SPINEL_RESET_CONNECTION=ON \
          -DOT_THREAD_VERSION=1.2 \
          -DOT_DAEMON=ON \
          -DOPENTHREAD_POSIX_CONFIG_DAEMON_SOCKET_BASENAME=/var/snap/openthread/%s \
          ..
      ninja
      DESTDIR=$CRAFT_PART_INSTALL ninja install
    override-prime: |
      craftctl default
      strip usr/bin/ot-ctl
      strip usr/sbin/ot-daemon

apps:
  ot-daemon:
    command: usr/sbin/ot-daemon
    plugs: [network, network-bind, serial-port, network-control]
  ot-ctl:
    command: usr/bin/ot-ctl
    plugs: [network]
